import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { DetailsPage } from '../details/details';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { key } from '../../app/tmdb';
import { AlertController } from 'ionic-angular';
import { Subscription } from 'rxjs/Subscription';
import { Platform } from 'ionic-angular';
import { Shake } from '@ionic-native/shake';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {
  results: Observable<Result[]>;
  pushPage: any;
  shakeSubscription: Subscription;


  constructor(public navCtrl: NavController, public http: HttpClient, public alertCtrl: AlertController, public platform: Platform, public shake: Shake) {
    this.results = Observable.of([]);
    this.pushPage = DetailsPage;
  }

  ionViewDidEnter(){
    this.shakeSubscription = Observable.fromPromise(this.platform.ready())
    .switchMap (()=>this.shake.startWatch())
    .switchMap (()=>this.discoverMovie("2018"))
    .subscribe(movies=>this.showRandomMovieAlert(movies));
  }

  ionViewWillLeave(){
    this.shakeSubscription.unsubscribe();
  }

  onInput($event: any) {
    let val = $event.target.value;
    if (val != '') {
      this.results = this.fetchResults(val);
    } else {
      this.results = Observable.of([]);
    }
  }

  fetchResults(search: string): Observable<Result[]> {
    return this.http.get<Result[]>("https://api.themoviedb.org/3/search/movie", {
      params: {
        api_key: key,
        query: search
      }
    }).pluck("results");
  }

  discoverMovie(year: string): Observable<Result[]>{
    return this.http.get<Result[]>("https://api.themoviedb.org/3/discover/movie", {
      params: {
        api_key: key,
        release_date: year
      }
    }).pluck("results");
  }

  showRandomMovieAlert(items: Result[]): void {
    var item = items[Math.floor(Math.random() * 10)];
    let confirm = this.alertCtrl.create({
      title: item.title,
      message: item.overview,
      buttons: [
        {
          text: 'Cancel',
          handler: () => {
          }
        },
        {
          text: 'Details',
          handler: () => {
            this.navCtrl.push(DetailsPage, item);
          }
        }
      ]
    });
    confirm.present();
  }
}

export interface Result {
  poster_path: string;
  adult: boolean;
  overview: string;
  release_date: string;
  genre_ids: number[];
  id: number;
  original_title: string;
  original_language: string;
  title: string;
  backdrop_path: string;
  popularity: number;
  vote_count: number;
  video: boolean;
  vote_average: number;
}

/*const fakeResults: Result[] = [
  {
    poster_path: 'https://findicons.com/files/icons/2713/mobile_device_icons/512/refresh.png',
    adult: true,
    overview: 'teubi',
    release_date: '12 décembre 2009',
    genre_ids: [3, 3],
    id: 2,
    original_language: 'fr',
    original_title: 'coucou',
    title: 'Test 3 : Trilogy',
    backdrop_path: 'coucou',
    popularity: 2,
    vote_count: 2,
    video: true,
    vote_average: 2
  }
];*/
